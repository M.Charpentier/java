import java.util.List;
import java.util.ArrayList;

class ExecutableMethodeAdd {
    public static void main(String [] args) {
        List<String> a = new ArrayList<>(); 
        // (Utiliser le Add)
        String john = "John";
        a.add(john);
        a.add("Jacques");
        List<String> b = new ArrayList<>();
        b.add("Jean");
        System.out.println(b);
        b.addAll(a);
        System.out.println(a);
        System.out.println(b);
        System.out.println(b.subList(0, 3));
    }
    
}
