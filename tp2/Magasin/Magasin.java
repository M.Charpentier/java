public class Magasin {
    private String nom;
    private boolean ouvertLundi, ouvertDimanche ;

    public Magasin(String nom, boolean lundi, boolean dimanche) {
        this.nom = nom;
        this.ouvertLundi = lundi;
        this.ouvertDimanche = dimanche;
    }
    // les getters
    public String getNom() {
        return this.nom;
    }
    public boolean getOuvertLundi(){
        return this.ouvertLundi;
    }

    public boolean getOuvertDimanche(){
        return this.ouvertDimanche;
    }

    @Override
    public String toString() { 
       String res = "Le magasin "+this.nom;
       if (this.ouvertLundi){
           res += " est ouvert le Lundi";
       }
       else{
           res += " est fermé le lundi";
       }
       if (this.ouvertDimanche){
           res += " et il est ouvert le dimanche.";
       }
       else{
           res += " et il est fermé le dimanche.";
       }
       return res;
    }
}