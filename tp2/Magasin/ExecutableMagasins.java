class ExecutableMagasins {
    public static void main(String [] args){
        Magasin magasin1 = new Magasin("Fleurus", true, false);
        Magasin magasin2 = new Magasin("BeauMagasin", true, true);
        Magasin magasin3 = new Magasin("Venir", false, false);
        Magasin magasin4 = new Magasin("Magnifique", false, true);
        ListeMagasins MaListeMagasins= new ListeMagasins();
        MaListeMagasins.ajoute(magasin1);
        MaListeMagasins.ajoute(magasin2);
        MaListeMagasins.ajoute(magasin3);
        MaListeMagasins.ajoute(magasin4);
        System.out.println(MaListeMagasins);
        System.out.println(MaListeMagasins.ouvertsLeLundi());
        System.out.println(magasin1.getOuvertLundi());
        System.out.println(magasin1.getOuvertDimanche());
    }
}