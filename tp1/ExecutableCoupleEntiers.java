class ExecutableCoupleEntiers {
    public static void main(String [] args) {
        CoupleEntiers couple = new CoupleEntiers();
        couple.setPrem(-16);
        System.out.println(couple); // (1)
        System.out.println(couple.fraction()); // (2)
        couple.setSec(-6);
        System.out.println(couple.getPrem()+" "+couple.getSec());
        System.out.println(couple.somme()); // affiche -22
        CoupleEntiers couple2 = new CoupleEntiers(5, 6);
        System.out.println(couple2);
        System.out.println(couple.plus(couple2)); 
    }
}