class CoupleEntiers {
    private int premier, second;
    public CoupleEntiers() {
        this.premier = 0;
        this.second = 0;
    }
    public CoupleEntiers(int premier, int  second) {
        this.premier = premier;
        this.second = second;
    }
    public String toString() {
        return "(" + this.premier + ", " + this.second + ")";
    }

    public void setPrem(int premier) {this.premier = premier;}

    public void setSec(int second) {this.second = second;}

    public int getPrem() {
        return this.premier;
    }

    public int getSec() {
        return this.second;
    }

    public void permute() {
        int aux;
        aux = this.premier;
        this.premier = this.second;
        this.second = aux;
    }
    public int fraction() {
        if (this.second != 0){
            return this.premier / this.second;
        }
        else {
            return this.premier ;
        }
    }
    public int somme() {
        return this.premier + this.second;
    }

    public CoupleEntiers plus(CoupleEntiers couple2) {
        CoupleEntiers couple3 = new CoupleEntiers( this.premier + couple2.getPrem(), this.second + couple2.getSec());
        return couple3;
    }
}