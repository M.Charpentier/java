public class Complexe {
    private int reel, imag;
    public Complexe(int reel, int imag)  {
        this.reel = reel;
        this.imag = imag;
    }
    public int getReel() {
        return this.reel;
    }
    public int getImag(){
        return this.imag;
    }
    public void afficheCartesien(){
        System.out.println("("+this.reel+", " + this.imag + ")");
    }
    public void afficheComplexe(){
        if (this.imag >= 0){
            System.out.println("  " + this.reel + "+" + this.imag + "i");
        }
        else{
            System.out.println(" "+this.reel + this.imag + "i");
        }
    }
    public Complexe Somme(Complexe complexe2){
        Complexe complexe3 = new Complexe(this.reel + complexe2.getReel(), this.imag + complexe2.getImag());
        return complexe3;
    }
}   
